<?php

/**
 * Front controller index.php
 * PHP 7.1.12
 */

/**
 * Composer autoloader
 */
require '../vendor/autoload.php';

/**
 * Set error and exception handling
 */
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');

/**
 * Routing
 */
$router = new Core\Router();

$router->add('', [
  'controller' => 'Home',
  'action' => 'index'
  ]
);

// $router->add('actors', [
//   'controller' => 'ActorController',
//   'action' => 'list'
// ]);

$router->add('{controller}/{action}');

// Get the requested route
$url = $_SERVER['QUERY_STRING'];

// Send requested route to router
$router->dispatch($url);