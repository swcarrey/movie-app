<?php

namespace App\Interfaces;

interface Api {
  public function getMoviesByDecade();
  public function getActorName();
}
