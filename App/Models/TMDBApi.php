<?php

namespace App\Models;

use \App\Config;
use \App\Interfaces\Api;

class TMDBApi implements Api {


  /** 
   * Brief bio details including ID, name, DOB etc.
   * 
   * @param array
   */
  private $actor_details = [];

  /**
   * Array of full credits including as cast
   * or producer or narrator etc.
   * 
   * @param array
   */
  private $movie_credits = [];

  /**
   * Array of movies as cast only
   * 
   * @param array
   */
  private $movies_as_cast = [];

  /**
   * Actor id
   * 
   * @param int
   */
  private $actor_id = null;

  /**
   * Actor name
   * 
   * @param string
   */
  private $actor_name;



  public function __construct($name) {
    if ($name) {
      $this->actor_name = $name;
      $key = $this->getAPIKey();

      $this->setActorDetails($key, $name);
      $this->setActorId();

      if ($this->actor_id !== null) {
        $this->setActorCredits($key, $this->actor_id);
        $this->setMoviesAsCast();
      }
    }
  }
  

  /**
   * Get the actor name
   * 
   * @return string
   */
  public function getActorName() {
    return $this->actor_name;
  }


  /**
   * Set the actor details
   * 
   * @param string $key The API key
   * @param string $name The actor name
   * 
   * @return void
   */
  private function setActorDetails($key, $name) {
    // Get actor ID
    $actor_details = file_get_contents("http://api.tmdb.org/3/search/person?api_key={$key}&query={$name}");
    $actor_details = json_decode($actor_details, true);
    
    // If there are results, set property
    if ($actor_details['total_results'] > 0) {
      $this->actor_details = $actor_details;
    }
  }


  /**
   * Set the actor ID
   * 
   * @return void
   */
  private function setActorId() {
    // Actor details is empty array if name search fails
    if ($this->actor_details) {
      $this->actor_id = $this->actor_details['results'][0]['id'];
    }
  }


  /**
   * Get full movie credits
   * 
   * @param string $key The API key
   * @param int $actor_id The ID of current searched actor
   * 
   * @return void
   */
  private function setActorCredits($key, $actor_id) {
    // Get all info from ID
    $movie_credits = file_get_contents("https://api.themoviedb.org/3/person/{$actor_id}/movie_credits?api_key={$key}&language=en-US");

    // Set property
    $this->movie_credits = json_decode($movie_credits, true);
  }


  /**
   * Set movies as cast only
   * 
   * @return void
   */
   private function setMoviesAsCast() {
    $this->movies_as_cast = $this->movie_credits['cast'];
  } 


  /**
   * Get movies as cast only
   * 
   * @return array
   */
  public function getMoviesAsCast() {
    return $this->movies_as_cast;
  } 



  /**
   * Get movies by decade
   * 
   * @return array
   */
  public function getMoviesByDecade() {
    $movies = $this->getMoviesAsCast();

    // Add custom image path to each movie
    $movies_with_path = [];
    foreach ($movies as $movie) {
      $movie['custom_image_path'] = 'http://image.tmdb.org/t/p/w185/' . $movie['poster_path'];
      array_push($movies_with_path, $movie);
    }

    // Generate arrays of movies with release date
    $with_date = $this->withReleaseDate($movies_with_path);

    // Sort movies with release date into ascending order
    usort($with_date, function($a, $b) {
      return strtotime($b["release_date"]) - strtotime($a["release_date"]);
    });
    
    // Movies without release dates
    $without_date = $this->withoutReleaseDate($movies_with_path);

    $result = [];
    foreach ($with_date as $movie) {
      $release_year = substr($movie['release_date'], 0, 4);
      $decade = $release_year - ($release_year % 10) . 's';
      
      $result[$decade][] = $movie;
    }

    // Sort array newest first
    krsort($result);

    // Add any movies without a release date to array
    if ($without_date) {
      $result['No release date'] = $without_date;
    }
    
    return $result;
  }

  private function withReleaseDate($movies) {
    return array_filter($movies, function($movie) {
      return (array_key_exists('release_date', $movie) && !empty($movie['release_date']));
    });
  }

  private function withoutReleaseDate($movies) {
    return array_filter($movies, function($movie) {
      // No key
      if (!array_key_exists('release_date', $movie)) {
        return true;
      }
      // Has key but no value
      if (array_key_exists('release_date', $movie) && empty($movie['release_date'])) {
        return true;
      }
      return false;
    });
  }



  /**
   * Get the API key for TMBD
   * 
   * @return string $key The API key
   */
  private function getAPIKey() {
    return Config::TMDB_API_KEY;
  }
}