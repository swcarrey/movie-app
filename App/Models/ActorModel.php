<?php

namespace App\Models;

/**
 * Actor model
 */
class ActorModel {
  /**
   * API object to be used
   * 
   * @param object
   */
  private $api;


  public function __construct($api) {
    $this->api = $api;
  }


  /**
   * Get actor name from actor details array if exists
   * 
   * @return string The actor name from TMDB
   */
  public function getFormattedActorName() {
    $name = $this->api->getActorName();

    return ucwords(str_replace('-', ' ', $name));
  }


  /**
   * Get movies by decade
   * 
   * @return array
   */
  public function getMoviesByDecade() {
    return $this->api->getMoviesByDecade();
  }

}

