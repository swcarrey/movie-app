<?php

namespace App\Utility;

abstract class Sanitizer {
  /**
   * Sanitize query variable string
   * 
   * @return string Sanitized string
   */
  public static function sanitize($string) {
    $string = str_replace(' ', '-', htmlspecialchars($string));
    $string = preg_replace('/[*&{}!^]/', '-', $string);

    return $string;
  }
}

