<?php

namespace App;

/**
 * Application configuration class
 */
class Config {

  /**
   * Database host
   * @var string
   */
  const DB_HOST = '';

  /**
   * Database name
   * @var string
   */
  const DB_NAME = '';

  /**
   * Database user
   * @var string
   */
  const DB_USER= '';

  /**
   * Database pass
   * @var string
   */
  const DB_PASS = '';

  /**
   * Show or hide error messages on screen
   * @var boolean
   */
  const SHOW_ERRORS = true;

  /**
   * TMDB API key
   */
  const TMDB_API_KEY = '';
}
