<?php

namespace App\Controllers;

// Bring View class into current namespace
use \Core\View;
use \Core\Controller;

/**
 * Home controller
 */
class Home extends Controller {
  /**
   * Show the index page
   * 
   * @return void
   */
  public function indexAction() {
    View::renderTemplate('Home/index.html'); 
  }
}
