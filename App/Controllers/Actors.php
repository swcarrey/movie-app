<?php

namespace App\Controllers;

// Bring Core/View into current namespace
use \Core\View;

use \App\Models\ActorModel;
use \App\Models\TMDBApi;
use \App\Utility\Sanitizer;
use \Core\Controller;

/**
 * About controller
 */
class Actors extends Controller {

  /**
   * Get all movies and send to view along with actor name
   * 
   * @return void
   */
  public function listAction() {
    // Sanitize name
    $name = Sanitizer::sanitize($_GET['name']);
    // New API object
    $api = new TMDBApi($name);
    
    // Create actor model object
    $actor = new ActorModel($api);

    $movies = $actor->getMoviesByDecade();
    $name = $actor->getFormattedActorName();


    View::renderTemplate('Actor/list-all.html', [
      'name' => $name,
      'movies' => $movies
    ]);
  }
}