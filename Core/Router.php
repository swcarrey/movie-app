<?php

namespace Core;

/**
 * Base router class
 */
class Router {
  /**
   * Associative array of routes (the routing table)
   * @var array
   */
  protected $routing_table = [];

  /**
   * Parameters from the matched route
   * @var array
   */
  protected $params = [];

  /**
   * Add a route to the routing table
   * 
   * @param string $route The route URL i.e. "{action}/{controller}"
   * @param array $params Parameters (cotroller, action, etc.)
   * 
   * @return void
   */
  public function add($route, $params = []) {
    // Generate regex to use for route array keys
    // Escape forward slashes
    $route = preg_replace('/\//', '\\/', $route);

    // Convert variables to regex e.g. {controller}
    $route = preg_replace('/\{([a-z]+)\}/', '(?P<\1>[a-z-]+)' , $route);

    // Convert variables with custom regular expressions e.g. {id:\d+}
    $route = preg_replace('/\{([a-z]+):([^\}]+)\}/', '(?P<\1>\2)', $route);

    // Add start and end delimiters and case insensitive flag
    $route = '/^' . $route . '$/i';

    // Save generated regex as key in routing table
    $this->routing_table[$route] = $params;
  }

  /**
   * Match the route to the routes in the routing table, setting the
   * $params property if the route is found
   * 
   * @param string $url The route URL
   * 
   * @return boolean true if match found, false otherwise
   */
  public function match($url) {
    // Loop the routing table as regex ($route) => array ($params)
    foreach ($this->routing_table as $route => $params) {
      // If URL matches route table key regex
      // Save matches in $matches
      if (preg_match($route, $url, $matches)) {

        // Loop over $matches preg_match output array
        foreach ($matches as $key => $match) {
          // Ignore index based e.g. [0], [1] etc. array items
          // Only get named capture groups
          if (is_string($key)) {
            $params[$key] = $match;
          }
        }

        $this->params = $params;
        return true;
      }
    }
    return false;
  }

  /**
   * Dispatch the route
   * 
   * @param string $url The route URL
   * 
   * @return void 
   */
  public function dispatch($url) {

    $url = $this->removeQueryStringVariables($url);

    if ($this->match($url)) {
      $controller = $this->params['controller'];
      $controller = $this->convertToStudlyCaps($controller);
      $controller = $this->getNamespace() . $controller;

      if (class_exists($controller)) {
        $controller_object = new $controller($this->params);

        $action = $this->params['action'];
        $action = $this->convertToCamelCase($action);

        // i.e. allow /index but not /indexAction
        if (preg_match('/action$/i', $action) == 0) {
          $controller_object->$action();
        }
        else {
          throw new \Exception("Method $action in controller $controller not found");
        }
      }
      else {
        //echo "Controller class $controller not found!";
        throw new \Exception("Controller class $controller not found!");
      }
    }
    else {
      //echo 'No route matched!';
      throw new \Exception("No route matched!", 404);
    }
  }

  /**
   * Get all the routes from the routing table
   * @return array
   */
  public function getRoutes() {
    return $this->routing_table;
  }

  /**
   * Get the curently matched $params
   * 
   * @return array
   */
  public function getParams() {
    return $this->params;
  }

  /**
   * Convert the string with hypens to studly caps
   * e.g. post-authors -> PostAuthors
   * 
   * @param string $string The string to convert
   * 
   * @return string 
   */
  protected function convertToStudlyCaps($string) {
    return str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
  }

  /**
   * Convert the string with hyphens to camel case
   * e.g. add-new -> addNew
   * 
   * @param string $string The string to convert
   * 
   * @return string 
   */
  protected function convertToCamelCase($string) {
    return lcfirst($this->convertToStudlyCaps($string));
  }

  /**
   * Remove the query string variables from the URL (if any). As the full
   * query string is used for the route, any variables at the end will need
   * to be removed before the route is matched to the routing table. For
   * example:
   *
   *   URL                           $_SERVER['QUERY_STRING']  Route
   *   -------------------------------------------------------------------
   *   localhost                     ''                        ''
   *   localhost/?                   ''                        ''
   *   localhost/?page=1             page=1                    ''
   *   localhost/posts?page=1        posts&page=1              posts
   *   localhost/posts/index         posts/index               posts/index
   *   localhost/posts/index?page=1  posts/index&page=1        posts/index
   *
   * A URL of the format localhost/?page (one variable name, no value) won't
   * work however. (NB. The .htaccess file converts the first ? to a & when
   * it's passed through to the $_SERVER variable).
   *
   * @param string $url The full URL
   *
   * @return string The URL with the query string variables removed
   */
  protected function removeQueryStringVariables($url) {
    if ($url != '') {
      // Convert to array (limited to 2 elements) before and after the & character
      $parts = explode('&', $url, 2);

      if (strpos($parts[0], '=') === false) {
        $url = $parts[0];
      }
      else {
        $url = '';
      }
    }
    return $url;
  }

  /**
   * Get the namespace for the controller class. The namespace
   * defined in the route parameters is added is present
   * 
   * @return string The request URL 
   */
  protected function getNamespace() {
    $namespace = 'App\Controllers\\';

    if (array_key_exists('namespace', $this->params)) {
      $namespace .= $this->params['namespace'] . '\\';
    }

    return $namespace;
  }
}
